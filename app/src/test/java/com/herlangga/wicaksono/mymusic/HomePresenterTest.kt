package com.herlangga.wicaksono.mymusic

import com.herlangga.wicaksono.mymusic.data.TestSchedulerProvider
import com.herlangga.wicaksono.mymusic.modules.home.HomeContract
import com.herlangga.wicaksono.mymusic.modules.home.HomePresenter
import com.herlangga.wicaksono.mymusic.network.Endpoint
import com.herlangga.wicaksono.mymusic.response.Music
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class HomePresenterTest {

    @Mock
    private lateinit var view: HomeContract.View

    @Mock
    private lateinit var api: Endpoint

    private lateinit var presenter: HomePresenter
    private lateinit var testScheduler: TestScheduler

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        testScheduler = TestScheduler()
        val testSchedulerProvider = TestSchedulerProvider(testScheduler)
        presenter = HomePresenter(view, testSchedulerProvider)
    }

    @Test
    fun testGetCuratedPhotos(){
        val mockedResponse: Music = mock()
        val term = "blink+182"
        Mockito.doReturn(Observable.just(mockedResponse)).`when`(api).search("",term, "musicTrack")

        presenter.search(term)
        testScheduler.triggerActions()

        Mockito.verify(view).showLoading()
        Mockito.verify(view).onSearchSuccess(mockedResponse)
        Mockito.verify(view).dismissLoading()
    }

    @Test
    fun testGetCuratedPhotosError() {
        val mockedResponse: Throwable = mock()
        val term = "blink+182"
        Mockito.doReturn(Observable.just(mockedResponse)).`when`(api).search("",term, "musicTrack")

        presenter.search(term)
        testScheduler.triggerActions()

        Mockito.verify(view).showLoading()
        Mockito.verify(view).onSearchFailed(mockedResponse.message.toString())
        Mockito.verify(view).dismissLoading()
    }

}