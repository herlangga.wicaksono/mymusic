package com.herlangga.wicaksono.mymusic

interface BaseView {

    fun showLoading()

    fun dismissLoading()

    fun showMessage(message: String)
}