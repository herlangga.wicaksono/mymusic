package com.herlangga.wicaksono.mymusic.modules.home

import com.base.util.SchedulerProvider
import com.herlangga.wicaksono.mymusic.network.HttpClient
import com.herlangga.wicaksono.mymusic.utils.extentions.getErrorBodyMessage
import io.reactivex.disposables.CompositeDisposable

class HomePresenter(private val view: HomeContract.View, private val scheduler: SchedulerProvider) :
        HomeContract.Presenter {

    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun subscribe() {}
    override fun unSubscribe() {
        mCompositeDisposable.clear()
    }

    override fun search(term: String) {
        view.showLoading()
        val disposable = HttpClient.getInstance().getApi().search("", term, "musicTrack")
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                        {
                            view.dismissLoading()
                            view.onSearchSuccess(it!!)
                        },
                        {
                            view.dismissLoading()
                            view.onSearchFailed(it.getErrorBodyMessage())
                        })
        mCompositeDisposable.add(disposable)
    }

}