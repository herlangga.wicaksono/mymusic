package com.herlangga.wicaksono.mymusic.widgets

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View

/**
 * this widget add margin to start
 * @param outReact.left to change margin start
 *
 * */
class RecyclerMarginLeft: androidx.recyclerview.widget.RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: androidx.recyclerview.widget.RecyclerView,
        state: androidx.recyclerview.widget.RecyclerView.State
    ) {
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.left = 32
        }
    }
}