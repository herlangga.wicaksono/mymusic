package com.herlangga.wicaksono.mymusic

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment(), BaseView {

    @LayoutRes
    protected abstract fun getFragmentView(): Int
    protected abstract fun onBindView(view: View)
    private lateinit var activity: BaseActivity

    private lateinit var fragmentHandler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity() as BaseActivity
        fragmentHandler = Handler(activity.mainLooper)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getFragmentView(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onBindView(view)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun showLoading() {
        activity.showLoading()
    }

    override fun dismissLoading() {
        activity.dismissLoading()
    }

    override fun showMessage(message: String) {
        activity.showMessage(message)
    }
}