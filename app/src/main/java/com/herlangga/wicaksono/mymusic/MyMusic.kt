package com.herlangga.wicaksono.mymusic

import android.util.Log
import androidx.multidex.BuildConfig
import androidx.multidex.MultiDexApplication

class MyMusic : MultiDexApplication() {

    companion object {
        lateinit var instance: MyMusic

        fun getApp(): MyMusic {
            return instance
        }

        fun log(message: String) {
            if (BuildConfig.DEBUG)
                Log.d("MyMusic", message)
        }
        fun logError(message: String) {
            if (BuildConfig.DEBUG)
                Log.e("MyMusic", message)
        }
        fun logError(message: Throwable) {
            if (BuildConfig.DEBUG)
                Log.e("MyMusic", "", message)
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}