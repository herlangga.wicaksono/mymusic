package com.herlangga.wicaksono.mymusic.utils

import android.content.Context
import android.text.TextUtils
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import com.herlangga.wicaksono.mymusic.R
import java.text.SimpleDateFormat
import java.util.*

object Helpers {
    fun getMaterialProgressDialog(context: Context): MaterialDialog {
        return MaterialDialog.Builder(context)
            .content(R.string.please_wait)
            .limitIconToDefaultSize()
            .title(R.string.loading)
            .autoDismiss(false)
            .canceledOnTouchOutside(false)
            .progress(true, 0)
            .build()
    }

    fun showToast(context: Context, message: String) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    fun getDefaultGson(): Gson {
        return GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .setDateFormat(Const.DATE_FORMAT_SERVER)
            .registerTypeAdapter(Date::class.java, JsonDeserializer { json, _, _ ->
                val formatServer = SimpleDateFormat(Const.DATE_FORMAT_SERVER, Locale.ENGLISH)
                formatServer.timeZone = TimeZone.getTimeZone("UTC")
                formatServer.parse(json.asString)
            })
            .registerTypeAdapter(Date::class.java, JsonSerializer<Date> { src, _, _ ->
                val format = SimpleDateFormat(Const.DATE_FORMAT_SERVER, Locale.ENGLISH)
                format.timeZone = TimeZone.getTimeZone("UTC")
                if (src != null) {
                    JsonPrimitive(format.format(src))
                } else {
                    null
                }
            })
            .create()
    }
}