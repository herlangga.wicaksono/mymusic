package com.herlangga.wicaksono.mymusic.utils.extentions

import android.view.View

fun View.visible() {
    if (isGone() || isInvisible()) visibility = View.VISIBLE
}

fun View.invisible() {
    if (isGone() || isVisible()) visibility = View.INVISIBLE
}

fun View.gone() {
    if (isInvisible() || isVisible()) visibility = View.GONE
}

fun View.isVisible(): Boolean {
    return this.visibility == View.VISIBLE
}

fun View.isInvisible(): Boolean {
    return this.visibility == View.INVISIBLE
}

fun View.isGone(): Boolean {
    return this.visibility == View.GONE
}