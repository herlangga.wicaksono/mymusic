package com.herlangga.wicaksono.mymusic.modules.home

import com.herlangga.wicaksono.mymusic.BasePresenter
import com.herlangga.wicaksono.mymusic.BaseView
import com.herlangga.wicaksono.mymusic.response.Music

interface HomeContract {
    interface View : BaseView {
        fun onSearchSuccess(data: Music)
        fun onSearchFailed(message: String)
    }

    interface Presenter : BasePresenter {
        fun search(term: String)
    }
}