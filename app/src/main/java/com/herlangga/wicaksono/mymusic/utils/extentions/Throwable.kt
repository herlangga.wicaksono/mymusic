package com.herlangga.wicaksono.mymusic.utils.extentions

import retrofit2.HttpException
import java.net.ConnectException
import java.net.UnknownHostException



fun Throwable.getErrorBodyMessage(): String {
    return if (this is HttpException) {
        "Permintaan anda belum berhasil di proses. Silakan coba kembali"
    } else if (this is ConnectException || this is UnknownHostException) {
        "Mohon periksa koneksi jaringan Anda"
    } else {
        return if (this.message == null)
            "Permintaan anda belum berhasil di proses. Silakan coba kembali"
        else
            this.message!!
    }
}