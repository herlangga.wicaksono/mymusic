package com.herlangga.wicaksono.mymusic

interface BasePresenter {
    fun subscribe()

    fun unSubscribe()
}