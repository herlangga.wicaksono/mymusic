package com.herlangga.wicaksono.mymusic.modules.home

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.base.util.AppSchedulerProvider
import com.herlangga.wicaksono.mymusic.BaseFragment
import com.herlangga.wicaksono.mymusic.MyMusic
import com.herlangga.wicaksono.mymusic.R
import com.herlangga.wicaksono.mymusic.modules.home.adapter.SearchListAdapter
import com.herlangga.wicaksono.mymusic.response.Music
import com.herlangga.wicaksono.mymusic.response.MusicResult
import com.herlangga.wicaksono.mymusic.utils.extentions.gone
import com.herlangga.wicaksono.mymusic.utils.extentions.visible
import com.herlangga.wicaksono.mymusic.widgets.RecyclerMarginLeft
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.layout_error.view.*
import java.util.*

class HomeFragment : BaseFragment(), HomeContract.View {

    private lateinit var presenter: HomePresenter
    private lateinit var listAdapter: SearchListAdapter
    private var timer: Timer = Timer()
    private var key = ""

    override fun getFragmentView(): Int = R.layout.fragment_home

    override fun onBindView(view: View) {
        val appSchedulerProvider = AppSchedulerProvider()
        presenter = HomePresenter(this, appSchedulerProvider)
        initAdapter()

        etSearchSuggestField.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) = timer.cancel()

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) = Unit

            override fun afterTextChanged(s: Editable) {

                timer = Timer()
                timer.schedule(object : TimerTask() {
                    override fun run() {
                        activity?.runOnUiThread {
                            if (s.toString().length >= 3) {
                                key = s.toString()
                                doSearch(key)
                            }
                        }
                    }
                }, 1000)
            }
        })

    }

    private fun doSearch(key: String){
        val term = key.replace(" ", "+").replace(".", "").replace("-", "").replace("_", "")
        presenter.search(term)
    }

    override fun onSearchSuccess(data: Music) {
        data.apply {
            if (!musicResults.isNullOrEmpty()) {
                rvCuratedList.visible()
                containerError.gone()
                listAdapter.addContent(musicResults!!)
            } else {
                setEmpty()
            }
        }
    }

    private fun setEmpty(message: String = getString(R.string.label_empty)) {
        rvCuratedList.gone()
        containerError.apply {
            visible()
            tvMessage.text = message
            tvTyrAgain.setOnClickListener {
                doSearch(key)
            }
        }
    }

    override fun onSearchFailed(message: String) {
        setEmpty(message)
    }

    private fun initAdapter() {
        listAdapter = SearchListAdapter(requireContext(), object : SearchListAdapter.Listener {
            override fun onSelected(selectedItem: MusicResult) {

            }
        })

        rvCuratedList.apply {
            layoutManager = LinearLayoutManager(context)
                    .also { it.orientation = LinearLayoutManager.VERTICAL }
            adapter = listAdapter
            isNestedScrollingEnabled = false
            addItemDecoration(RecyclerMarginLeft())
        }
    }
}