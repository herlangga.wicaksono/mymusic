package com.herlangga.wicaksono.mymusic.response
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Music(
    @Expose @SerializedName("resultCount")
    var resultCount: Int?,
    @Expose @SerializedName("results")
    var musicResults: List<MusicResult>?
)

data class MusicResult(
    @Expose @SerializedName("artistId")
    var artistId: Int?,
    @Expose @SerializedName("artistName")
    var artistName: String?,
    @Expose @SerializedName("artistViewUrl")
    var artistViewUrl: String?,
    @Expose @SerializedName("artworkUrl100")
    var artworkUrl100: String?,
    @Expose @SerializedName("artworkUrl30")
    var artworkUrl30: String?,
    @Expose @SerializedName("artworkUrl60")
    var artworkUrl60: String?,
    @Expose @SerializedName("collectionArtistId")
    var collectionArtistId: Int?,
    @Expose @SerializedName("collectionArtistName")
    var collectionArtistName: String?,
    @Expose @SerializedName("collectionArtistViewUrl")
    var collectionArtistViewUrl: String?,
    @Expose @SerializedName("collectionCensoredName")
    var collectionCensoredName: String?,
    @Expose @SerializedName("collectionExplicitness")
    var collectionExplicitness: String?,
    @Expose @SerializedName("collectionId")
    var collectionId: Int?,
    @Expose @SerializedName("collectionName")
    var collectionName: String?,
    @Expose @SerializedName("collectionPrice")
    var collectionPrice: Double?,
    @Expose @SerializedName("collectionViewUrl")
    var collectionViewUrl: String?,
    @Expose @SerializedName("contentAdvisoryRating")
    var contentAdvisoryRating: String?,
    @Expose @SerializedName("country")
    var country: String?,
    @Expose @SerializedName("currency")
    var currency: String?,
    @Expose @SerializedName("discCount")
    var discCount: Int?,
    @Expose @SerializedName("discNumber")
    var discNumber: Int?,
    @Expose @SerializedName("isStreamable")
    var isStreamable: Boolean?,
    @Expose @SerializedName("kind")
    var kind: String?,
    @Expose @SerializedName("previewUrl")
    var previewUrl: String?,
    @Expose @SerializedName("primaryGenreName")
    var primaryGenreName: String?,
    @Expose @SerializedName("releaseDate")
    var releaseDate: String?,
    @Expose @SerializedName("trackCensoredName")
    var trackCensoredName: String?,
    @Expose @SerializedName("trackCount")
    var trackCount: Int?,
    @Expose @SerializedName("trackExplicitness")
    var trackExplicitness: String?,
    @Expose @SerializedName("trackId")
    var trackId: Int?,
    @Expose @SerializedName("trackName")
    var trackName: String?,
    @Expose @SerializedName("trackNumber")
    var trackNumber: Int?,
    @Expose @SerializedName("trackPrice")
    var trackPrice: Double?,
    @Expose @SerializedName("trackTimeMillis")
    var trackTimeMillis: Int?,
    @Expose @SerializedName("trackViewUrl")
    var trackViewUrl: String?,
    @Expose @SerializedName("wrapperType")
    var wrapperType: String?
)