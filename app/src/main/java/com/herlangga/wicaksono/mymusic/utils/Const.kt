package com.herlangga.wicaksono.mymusic.utils

object Const {
    const val DATE_FORMAT_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val REQUEST_CODE_ERROR = 1
}