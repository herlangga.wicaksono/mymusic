package com.herlangga.wicaksono.mymusic.network

import com.herlangga.wicaksono.mymusic.response.Music
import retrofit2.http.GET
import io.reactivex.Observable
import retrofit2.http.Path
import retrofit2.http.Query

interface Endpoint {

    @GET("search")
    fun search(@Query("country") country: String, @Query("term") term: String, @Query("entity") entity: String): Observable<Music>
}