package com.herlangga.wicaksono.mymusic.utils.extentions

import android.annotation.SuppressLint
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.herlangga.wicaksono.mymusic.R

private const val placeholderImage: Int = R.drawable.ic_image_not_available

fun ImageView.loadImage(url: String?, isSaveCache: Boolean = true, placeholder: Int = placeholderImage) {
    Glide.with(this.context)
            .load(url)
            .apply(if (isSaveCache) requestOptionStandart(placeholder) else requestOptionStandartNoSaveCache(placeholder))
            .into(this)
}

@SuppressLint("CheckResult")
fun requestOptionStandart(placeholder: Int = placeholderImage): RequestOptions {
    val requestOptions = RequestOptions()
    requestOptions.placeholder(placeholder)
    return requestOptions
}

@SuppressLint("CheckResult")
fun requestOptionStandartNoSaveCache(placeholder: Int = placeholderImage): RequestOptions {
    val requestOptions = RequestOptions()
    requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE)
    requestOptions.skipMemoryCache(true)
    requestOptions.placeholder(placeholder)
    return requestOptions
}