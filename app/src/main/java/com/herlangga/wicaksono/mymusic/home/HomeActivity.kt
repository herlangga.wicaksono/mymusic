package com.herlangga.wicaksono.mymusic.modules.home

import com.herlangga.wicaksono.mymusic.BaseActivity
import com.herlangga.wicaksono.mymusic.R

class HomeActivity : BaseActivity() {
    
    override fun getActivityView(): Int = R.layout.activity_home

    override fun onBindView() {

    }
}