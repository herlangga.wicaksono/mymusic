package com.herlangga.wicaksono.mymusic.modules.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.herlangga.wicaksono.mymusic.R
import com.herlangga.wicaksono.mymusic.response.MusicResult
import com.herlangga.wicaksono.mymusic.utils.extentions.loadImage
import kotlinx.android.synthetic.main.item_music_list.view.*


class SearchListAdapter(private val context: Context, private val listener: Listener) :
        RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {

    private var content: MutableList<MusicResult> = mutableListOf()

    fun addContent(content: List<MusicResult>) {
        this.content.addAll(content)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(
                    LayoutInflater.from(context).inflate(
                            R.layout.item_music_list,
                            parent,
                            false
                    )
            )

    override fun getItemCount(): Int = content.size

    override fun onBindViewHolder(holder: SearchListAdapter.ViewHolder, position: Int) {
        holder.bindItem(content[position])
    }

    interface Listener {
        fun onSelected(selectedItem: MusicResult)
    }

    inner class ViewHolder(itemView: View)
        : RecyclerView.ViewHolder(itemView) {
        fun bindItem(
                data: MusicResult
        ) {
            itemView.apply {
                data.apply {
                    ivPhoto.loadImage(artworkUrl100, false)
                    tvValue.text = trackName
                    tvTitle.text = artistName
                    setOnClickListener {
                        listener.onSelected(this)
                    }
                }
            }
        }
    }
}