package com.herlangga.wicaksono.mymusic

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.afollestad.materialdialogs.MaterialDialog
import com.herlangga.wicaksono.mymusic.R
import com.herlangga.wicaksono.mymusic.utils.Helpers

abstract class BaseActivity : AppCompatActivity(), BaseView {

    private lateinit var progressDialog: MaterialDialog

    @LayoutRes
    protected abstract fun getActivityView(): Int

    protected abstract fun onBindView()

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        progressDialog = Helpers.getMaterialProgressDialog(this)
        setContentView(getActivityView())

        onBindView()
    }

    override fun showLoading() {
        progressDialog.show()
    }

    override fun dismissLoading() {
        progressDialog.dismiss()
    }

    override fun showMessage(message: String) {
        Helpers.showToast(this, message)
    }
}