package com.herlangga.wicaksono.mymusic.network

import com.herlangga.wicaksono.mymusic.MyMusic
import com.herlangga.wicaksono.mymusic.BuildConfig
import com.herlangga.wicaksono.mymusic.utils.Helpers
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class HttpClient {
    private lateinit var client: Retrofit
    private lateinit var endpoint: Endpoint

    companion object {
        private val mInstance: HttpClient = HttpClient()

        @Synchronized
        fun getInstance(): HttpClient {
            return mInstance
        }
    }

    init {
        buildRetrofitClient()
    }

    fun getApi(): Endpoint {
        endpoint = client.create(Endpoint::class.java)
        return endpoint
    }

    private fun buildRetrofitClient() {
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(2, TimeUnit.MINUTES)
        builder.readTimeout(2, TimeUnit.MINUTES)

        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
            builder.addInterceptor(ChuckInterceptor(MyMusic.getApp()))
        }

//        builder.addInterceptor(getInterceptorWithHeader("Authorization", BuildConfig.API_KEY))

        val okHttpClient = builder.build()
        client = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(Helpers.getDefaultGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    /**
     * get [Interceptor] with single header
     * @param headerName [String] of header name
     *
     * @param headerValue [String] of header value
     *
     * @return Interceptor with header
     */
    private fun getInterceptorWithHeader(headerName: String, headerValue: String): Interceptor {
        val header = HashMap<String, String>()
        header[headerName] = headerValue
        return getInterceptorWithHeader(header)
    }

    /**
     * get [Interceptor] with multiple header

     * @param headers set header name to key and header value to value
     * *
     * @return Interceptor with headers
     */
    private fun getInterceptorWithHeader(headers: Map<String, String>): Interceptor {
        return Interceptor { chain ->
            val original = chain.request()
            val builder = original.newBuilder()
            for ((key, value) in headers) {
                builder.addHeader(key, value)
            }
            builder.method(original.method(), original.body())
            chain.proceed(builder.build())
        }
    }
}