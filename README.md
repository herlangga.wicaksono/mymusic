# MyMusic

## Support Device
<details><summary>minSdkVersion 25</summary>
</details>

### Support Features.
<details><summary>Music Listing</summary>
The sources of data is from iTunes affiliate API
</details>

#### Requirements to build the app.
<details><summary>All of the requirement is listed on gradle file</summary>
kotlin_version = "1.3.50"
mockito_version = "2.19.0"
archLifecycleVersion = '2.0.0'
espressoVersion = "3.4.0"
chuck interceptor = "1.1.0"
</details>

##### Instructions to build and deploy the app.
<details><summary>Project preparation</summary>
Open this project on Android Studio
</details>

<details><summary>Deployment</summary>
make sure you have an (virtual) Andrid Device that is connected to the pc, then click "play" icon to deploy the app.
</details>